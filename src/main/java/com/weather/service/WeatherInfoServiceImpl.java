package com.weather.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.weather.util.Constants;

@Service
public class WeatherInfoServiceImpl implements WeatherInfoService {

	@Override
	public Object getWeatherInfo(Integer zipCode) {
		String result = null;
		try {
			URL url = new URL(
					"https://weather.api.here.com/weather/1.0/report.json?app_id="+Constants.APP_ID+"&app_code=%20"+Constants.APP_CODE+"%20&product=forecast_hourly&metric=false&zipcode="
							+ zipCode);
			url.openStream().close();
			URLConnection con = url.openConnection();
			InputStream is = con.getInputStream();
			BufferedReader ar = new BufferedReader(new InputStreamReader(is));
			String line = null;
			line = ar.readLine();
			System.out.println("Record ---> "+ line);
			JSONObject obj = null;
			obj = new JSONObject(line);
			System.out.println("Record ---> "+ obj);
			JSONArray arr = null;
			arr = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getJSONArray("forecast");
			String cityName = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getString("city");
			ArrayList<Double> temp = new ArrayList<Double>();
			ArrayList<String> dateTime = new ArrayList<String>();
			int i = 0;
			String today = arr.getJSONObject(i).getString("weekday");
			for (i = 1; today.equals(arr.getJSONObject(i).getString("weekday")); i++) {
			}
			for (int j = 0; j < 24; i++, j++) {
				temp.add(Double.parseDouble((arr.getJSONObject(i).getString("temperature"))));
				dateTime.add(arr.getJSONObject(i).getString("localTime"));
			}
			int min_index = temp.indexOf(Collections.min(temp));
			String time = dateTime.get(min_index).substring(0, 2);
			String date = dateTime.get(min_index).substring(2, dateTime.get(min_index).length());
			date = date.substring(0, 2) + "-" + date.substring(2, 4) + "-" + date.substring(4, 8);
			String meridiem = "am";
			// Display time in meridiem
			if (Integer.parseInt(time) > 11) {
				if (time != "12")
					time = Integer.toString((Integer.parseInt(time) - 12));
				meridiem = "pm";
			} else {
				if (time == "00")
					time = "12";
			}
			result = "coolest hour in " + cityName + " tomorrow on " + date + " would be at " + time + " " + meridiem;

		} catch (Exception e) {
			System.out.println("Exception in " + this.getClass().getName());
		}
		return result;
	}

}
