package com.weather.service;

public interface WeatherInfoService {
	
	public Object getWeatherInfo(Integer zipCode);

}
