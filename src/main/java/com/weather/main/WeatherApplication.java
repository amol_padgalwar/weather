package com.weather.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
//scanBasePackages = {"com.weather.controller"}
@SpringBootApplication()
@ComponentScan({"com.weather.controller","com.weather.service"})
public class WeatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
	}

}
