package com.weather.dto;

public class Response {
	
	private String statusCode;
	private String message;
	private Object coolestHour;
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getCoolestHour() {
		return coolestHour;
	}
	public void setCoolestHour(Object coolestHour) {
		this.coolestHour = coolestHour;
	}
	

}
