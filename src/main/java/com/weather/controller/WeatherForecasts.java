package com.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weather.dto.Response;
import com.weather.service.WeatherInfoService;

@RestController
@RequestMapping("/api")
public class WeatherForecasts {

	@Autowired()
	WeatherInfoService weatherInfoService;

	@RequestMapping(value = "/getWeatherForecasts/{zipCode}", method = RequestMethod.GET)
	public ResponseEntity<Response> getWeatherDetails(@PathVariable("zipCode") Integer zipCode) {
		Response res = new Response();
		Object obj = null;
		if (zipCode != null && zipCode > 0) {
			obj = weatherInfoService.getWeatherInfo(zipCode);
			if (obj != null) {
				res.setMessage("success");
				res.setStatusCode(HttpStatus.OK.value() + "");
				res.setCoolestHour(obj);
				return new ResponseEntity<Response>(res, HttpStatus.OK);
			} else {
				res.setCoolestHour(obj);
				res.setMessage("ZipCode is Not Valid");
				res.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");
				return new ResponseEntity<Response>(res, HttpStatus.BAD_REQUEST);
			}
		}
		res.setCoolestHour(obj);
		res.setMessage("fail");
		res.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");
		return new ResponseEntity<Response>(res, HttpStatus.BAD_REQUEST);

	}
}
